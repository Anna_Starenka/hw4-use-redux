import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { dataFetch } from "./reducers";
import { Routes, Route } from 'react-router-dom';

import Header from "./Components/Header/Header";
import Home from "./pages/Home/Home";
import Cart from "./pages/Cart/Cart";
import Favourite from "./pages/Favorite/FavouriteI";

import './App.css'


function App() {

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(dataFetch());
    }, [dispatch])

    return (
        <>

            <div className="container">
                <Header />
                <Routes>  
                    <Route path="/" element={<Home />} />
                    <Route path="/cartitems" element={<Cart/>} />
                    <Route path="/favourites" element={<Favourite />} />
                    <Route path='*' element={<p>page is not found</p>} />
                </Routes>
            </div>
        </>

    )

};

export default App