import ConfirmButton from "../Buttons/ConfirmButton/ConfirmButton";
import "./Modal.scss";

export default function Modal({ text, onCancel, onConfirm }) {
  return (
    <div className="total-wrapper" onClick={onCancel}>
      <div
        className="modal-wrapper"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div className="main-wrapper">{text}</div>
        <div className="btn-wrapper">
          <ConfirmButton className="ok-btn" text="ok" onClick={onConfirm} />
          <ConfirmButton
            className="cancel-btn"
            text="cancel"
            onClick={onCancel}
          />
        </div>
      </div>
    </div>
  );
}
