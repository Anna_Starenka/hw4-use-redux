import './AddToList.scss'

export default function AddToList({ text,onClick }) {

    return (
        <>
            <div className="btn-wrapper">
                <button className='add-btn' onClick={onClick}>{text}</button>
            </div>
        </>

    )

};


