import Modal from "../../Components/Modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import { removeFromCart, modalClose, modalOpen } from "../../reducers";

import "./cart.scss";

export default function Cart() {
  const dispatch = useDispatch();

  const modal = useSelector((state) => state.modal.isModal);
  const cart = useSelector((state) => state.cart.cartToLocal);

  return (
    <>
      {!cart.length && (
        <div className="cart-empty">
          <strong> Cart is empty</strong>
        </div>
      )}
      <h1 className="title-main">Cart</h1>
      <div className="products-wrapper">
        {cart.map((el) => (
          <div key={el.id}>
            <div className="card-wrapper">
              <div className="image-wrapper">
                <img className="image" src={el.image} alt={el.name} />
              </div>
              <div className="flex-wrapp">
                <h1>{el.name}</h1>

                <div
                  className="remove-card"
                  onClick={() => {
                    dispatch(modalOpen());
                  }}
                >
                  X
                </div>
              </div>

              <div className="price-wrapp">
                <p className="price">UAH {el.price}</p>
                <p className="art">Art: {el.article}</p>
                <p className="color">Color: {el.color}</p>

                {modal && (
                  <Modal
                    text="Do you want to delete this product from cart?"
                    onCancel={() => {
                      dispatch(modalClose());
                    }}
                    onConfirm={() => {
                      dispatch(removeFromCart(el));
                      dispatch(modalClose());
                    }}
                  />
                )}
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}
